---
title: Editing Entities
template: main-repo.html
---

What if our user wants to edit the data?

Let's take a look at editing a `Movie`.

We define a `MovieEdit` function (we'll talk about it in detail later) that takes a `movieId` and lets you edit its `title` and `description`. We won't be changing any roles.

Because of this, we only need to fetch the `Movie` itself, and not the `MovieWithCast`.

## The Data Layer

We've already got an `update` function for our `Movie`, but we're going to need a way to fetch a `Movie` by itself...

This is trivial using Room (in the `MovieDAO`)

```kotlin
@Query("SELECT * FROM Movie WHERE id = :id")
abstract suspend fun getMovie(id: String): Movie
```

We propagate it up through the repository to make it accessible to the UI layer.

## Updating DTOs

The DTO objects that we work with are immutable data classes. We must create a new instance when we want to change it. Kotlin `data classes` generate a `copy` function that looks like (for `MovieDto`)

```kotlin
fun copy(
    id: String = this.id,
    name: String = this.name,
    age: Int = this.age,
) = MovieDto(
    id = id,
    name = name,
    age = age
)
```

This wonderful little function allows you to pass _only_ the properties that you want to change; everything else gets the current value. You use it like:

```kotlin
val newMovie = movie.copy(name = newName)
```

We'll use this to create new DTOs that we pass to the `update` function in the repository.

## Auto update vs User-Triggered

There are two main approaches to updating the data in the data layer.

   * Wait for the user to indicate they're done, then save the data.
      * This could be an explicit "done" button.
         * Pressing "done" calls update once and exits the edit screen
         * Pressing "cancel" or "back" just exits (but typically presents a dialog asking "are you sure you want to lose your changes?")
      * This could be done by listening for "back", and then save
         * Might have a cancel button as well that backs out without saving data
      * This is simpler to code, as you have an explicit spot to perform updates and don't need to worry too much about how long the save takes and if it competes with other saves
   * Update the data as the user is typing it
      * This is especially useful if the data appears in multiple parts of the UI. For example, a screen that has a "list" of items next to a "detail" pane that's letting you edit the data. As you type, you'd see the values change in the list as well as the fields you're typing.
      * The concern is that this would cause many updates, which could cause performance issues or excessive network data use.
      * It's possible for multiple fast updates to run at the same time, out of order.
      
For the class examples, we'll be using the "update as the user types" approach, with some "debouncing" to help ordering, performance and network data use.

## View Model Changes
    
First - note that we've added a new `Screen` type named `MovieEdit`. We'll use this as a navigation target.

The basic update that we want to make looks like

```kotlin
fun updateMovie(movieDto: MovieDto) {
    viewModelScope.launch {
        repository.update(movieDto)
    }
}
```

We start a new coroutine to pass our new DTO to `update` in the repository.

But this can cause some nasty issues...

If the updates are sent quickly (and/or the updates take a while behind the scenes, such as a slow web service call), we could have multiple coroutines in play at the same time.

When that happens, they can run in any order. Think about what happens if you type "abc" in a field and want to update the object on the fly. We could get three updates running at the same time for values

   * "a"
   * "ab"
   * "abc"

If they run in that order, all is well. But if they run in another order, we could end up with data stored in the database that doesn't match what the user wanted.

And if they _did_ run in the correct order, we could have more database or network updates than were really needed.

To fix this, we can "debounce" the updates. The idea here is that we won't actually call `update` in the repository unless the user has been idle for a period of time. If they're typing, we would get an update request to the view model for each letter, but wait until the user pauses before sending the current update to the repository.

An easy way to do this is to track the last-run job and cancel if a new job comes in before it's finished. Adding a `delay` at the start of the coroutine allows the cancel to take effect if a second coroutine is launched before the delay completes. This looks like

```kotlin
private var movieUpdateJob: Job? = null

fun updateMovie(movieDto: MovieDto) {
    movieUpdateJob?.cancel()
    movieUpdateJob = viewModelScope.launch {
        delay(500)
        repository.update(movieDto)
        movieUpdateJob = null
    }
}
```

The `movieUpdayteJob` variable keeps track of the last-launched job. If another update is requested, it cancels the previous coroutine before starting a new coroutine.

The coroutine delays for 500ms. If more updates come in, with less than 500ms between them, the current job will keep getting canceled. Once the user pauses for at least 500ms, the update will be sent to the repository.

## MovieScaffold/MovieDisplay Changes

We need some way for the user to enter the `MovieEdit` screen. I'm placing an edit icon (looks like a pencil) on the top app bar when the `MovieDisplay` screen is displayed.

To do this, I've updated the `MovieScaffold` by adding an `onEdit` parameter:

```kotlin
@Composable
fun MovieScaffold(
    ...
    onEdit: (() -> Unit)? = null,
    ...
) {
    ...
}
```

This parameter is a _nullable_ function that takes no parameters and doesn't return anything. We default it to null; if the caller (one of the screen composable functions) doesn't specify it, it'll be null.

Inside the function we set up an edit on the top app bar if `onEdit` isn't null:

```kotlin
TopAppBar(
    ...
    actions = {
        onEdit?.let { onEdit ->
            IconButton(
                onClick = onEdit,
                modifier = Modifier.padding(8.dp),
            ) {
                Icon(
                    imageVector = Icons.Default.Edit,
                    contentDescription = stringResource(id = R.string.edit),
                )
            }
        }
        ...
    }
    ...
}
```

Then we call `MovieScaffold` from `MovieDisplay`:

```kotlin
@Composable
fun MovieDisplay(
    ...
    onEdit: (String) -> Unit,
) {
    var movieWithCastDto by remember { mutableStateOf<MovieWithCastDto?>(null) }

    LaunchedEffect(key1 = movieId) {
        // starts a coroutine to fetch the rating
        movieWithCastDto = fetchMovieWithCast(movieId)
    }

    MovieScaffold(
        ...
        onEdit =
            movieWithCastDto?.let { movieWithCast ->
                {
                    onEdit(movieWithCast.movie.id)
                }
            }
    ) {
        ...
    }
}
```

Once the movie has been loaded, we create a lambda with the `onEdit` call inside, and pass that to `onEdit` in the `MovieScaffold`.

The Ui composable manages the navigation and adds the call to the new `MovieEdit` screen:

```kotlin
when(val screen = viewModel.screen) {
    ...
    is MovieDisplay -> MovieDisplay(
        ...
        onEdit = { id ->
            viewModel.pushScreen(MovieEdit(id))
        }
    )
    is MovieEdit -> MovieEdit(
        ...
    )
}
```

## MovieEdit

Now comes the tricky part. At least it's a little tricky to describe...

Jetpack Compose's TextField is the composable function that declares your text input field. We'll be using OutlinedTextField, which gives a border with a nice label for the input.

Using `OutlinedTextField`, our `MovieEdit` screen will look like

![MovieEditScreen](screenshots/textfields.png)

Compose's `TextField` (and `OutlinedTextField`) take two main parameters

   * `value` - the current value to display
   * `onValueChange` - the event function that's called when `TextField` sees the user change the value.

The `TextField` itself will _not_ automatically update the displayed value. Rather, it calls `onValueChange`, and you'll pass in a new value. This gives you a chance to format or validate the value. But it also means you'll need to store that value somewhere.

You _could_ just pass in `movie.title` for `value`, and a lambda that updates the `Movie` in the database. However, if we debounce, we won't actually **see** that value in the text field until the user stops typing.

If we didn't debounce, the user's typing might outpace the data updates. Sometimes the updates get out of order, which can cause the wrong values to be displayed. And sometimes this causes a race between the caret position in the text field and the value being updated. I've seen some interesting situations where I'm typing quickly (using a physical keyboard) and suddenly the caret is in the middle of the text and my typing continues there. Not good.

What do we do?

   * Use `LaunchedEffect` like we do in `MovieDisplay` to fetch the `Movie` whenever
     the `movieId` changes. Note that if we update the `title` and `description` values
     in the database, the `movieId` _does not change_.
   * Pass `movie?.title ?: ""` to the `TextFields`. This will pass a title in if we 
     have one, or and empty string if the movie is null (thanks to the Elvis operator!).
   * Whenever we get `onValueChange` from the text field
      * Create a copy of the remembered `movie` by changing the field that we're editing.
      * Update the `remember`ed value so we always have the most up-to-date values available
      * Ask to update the movie value in the database. Note that this might take a while or be debounced. That's ok; the user is editing with a local copy of the
      movie that will always be consistent with their typing.

Keep in mind that the `mutableStateOf` creates a Jetpack Compose `MutableState`. I like to think of it as a "bucket" that you can drop things into, and which Compose observes for changes. When a new value is dropped into the bucket, Compose will recompose anything that read from that bucket. The `remember` holds that bucket in the composition tree so it lives across recompositions (unless its parent node were removed from the composition - for example, if you were displaying two movies on the screen and one were removed, its remembered bucket would be removed with it).

The relevant parts of `MovieEdit` look like

```kotlin
@Composable
fun MovieEdit(
    movieId: String,
    fetchMovie: suspend (String) -> MovieDto,
    ...
    onMovieUpdate: (MovieDto) -> Unit,
) {
    var movie by remember { mutableStateOf<MovieDto?>(null) }

    LaunchedEffect(key1 = movieId) {
        movie = fetchMovie(movieId)
    }

    MovieScaffold(...) { paddingValues ->
        Column(...) {
            TextEntry(
                ...
                value = movie?.title ?: "",
                onValueChange = {
                    movie = movie?.copy(title = it)?.apply {
                        onMovieUpdate(this)
                    }
                },
            )
            ... // similar for description field
        }
    }
}
```
