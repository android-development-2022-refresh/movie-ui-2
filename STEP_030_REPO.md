---
title: Cleanup the display screens
template: main-repo.html
---

Let's make the display screens a little nicer, using `LazyColumn`. For example, a movie display screen will look like

![Movie Detail Screen](screenshots/detail%20cleanup.png)

We've added the movie description, a `Divider` and a `LazyList` of the cast.

Here's the composition code for the movie display screen:

```kotlin
MovieScaffold(
    ...
) { paddingValues ->
    Column(
        modifier = Modifier
            .padding(paddingValues)
            .fillMaxHeight()                                // AAA
    ) {
        movieWithCastDto?.let { movieWithCast ->
            SimpleText(                                     // BBB
                text = movieWithCast.movie.description
            )
            Divider()                                       // CCC
            SimpleText(                                     // DDD
                text = stringResource(id = R.string.cast)
            )
            LazyColumn(                                     // EEE
                modifier = Modifier
                    .padding(8.dp)
                    .weight(1f) // take up the rest of the screen
            ) {
                items(
                    items = movieWithCast.cast,
                ) { roleWithActor ->
                    Card(...)
                }
            }
        }
    }
}
```

| Line | Description   |
|------|---------------|
| AAA  | Because we're adding in a `LazyColumn`, which controls its own scrolling, we no longer want the `Column` to scroll. The `Column` now fills the entire height that its parent allows. The fixed composables (description, divider and "cast") take up as much height as they need. The `LazyColumn` has a `weight` modifier which gives it all of the remaining space |
| BBB<br/>CCC<br/>DDD  | Added description, a divider line, and a header for the "cast" section. |
| EEE  | Note that our `LazyColumn` takes up the remainder of the screen and manages scrolling in that section | 

!!! note

    If the "fixed" content is likely to be too big for the screen, or leave so little space that the cast list wouldn't be usable, you could keep the overall `Column` as scrollable and add a "Cast" button/link in place of the `LazyColumn`. Pressing it would open a separate screen for the "Cast" when it's pressed. (The IMDb app does something like this, as there is a lot of fixed-size data on the main screen for a movie, which requires overall scrolling). If you have User Experience Designers available to your team, talk with them about how to best use your screen space to give the user a great experience.

The Rating display screen shows an interesting wrinkle. What do you do when a list is empty? One option is to display alternative composables. This is easy to do; just add an `if`/`else` expression!

```kotlin
if (ratingWithMovies.movies.isEmpty()) {
    SimpleText(text = stringResource(id = R.string.no_movies_found_for_this_rating))
} else {
    ... // normal display of header and LazyColumn
}
```

This is a great usability enhancement over the old "View" system. We used to have to either

   * dynamically add/remove views from the UI, or
   * define _all possible_ views that we might need, and show/hide them

Using logical expressions inside our composable functions makes changing the UI quite simple.