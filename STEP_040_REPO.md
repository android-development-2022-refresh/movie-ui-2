---
title: Refactor the list screens
template: main-repo.html
---

Did you notice how much repeated code exists across the main list screens? It's pretty heavy right now, and it's going to get worse when we add selection code. 

So, we refactor the common code into `ListScaffold`.

The main things we had to do to get this to work:

   * Generify the names to "item" rather than "movie", "actor" and "rating"
   * Add a generic type parameter to represent the type of Dto we're using
   * Add a `getId` function parameter that allows us to pass in an item and get its `id`. We don't have any idea what the type of the list items are, so we can either
      * Pass in a resolution function like this, or
      * Add an interface `HasId` and make each type we care about implement it. `HasId` only contains an `id` property. We'd also need to change the generic type parameter to `<T: HasId>` to restrict the caller to only passing in types that implement it _and_ allow the innards of the function to know that they can call `.id` on the item.
    * Add in item icons/content description and a page title.

Because we'll soon be using an icon to help in selection, we're adding it here. The `ListScaffold` will manage the clickable icon inside a `Row`, followed by the rest of the content that the caller wants to use to display each item.

!!! note

    We define the `itemContent` parameter as 

    ```kotlin
    itemContent: @Composable RowScope.(T) -> Unit
    ```

    This is a "slot" that we require the caller to pass in. We'll provide it a `RowScope` receiver, so they can just continue filling in the row after the icon. We pass in the item for that row as a parameter.

Now the caller lists are much simpler. For example:

```kotlin
@Composable
fun ActorList(
    actors: List<ActorDto>,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    onActorClick: (String) -> Unit,
) = ListScaffold(
    titleId = R.string.screen_title_actors,
    items = actors,
    getId = { it.id },
    onSelectListScreen = onSelectListScreen,
    onResetDatabase = onResetDatabase,
    onItemClick = onActorClick,
    itemIcon = Icons.Default.Person,
    itemIconContentDescriptionId = R.string.tap_to_toggle_selection,
) { actor ->
    SimpleText(text = actor.name)
}
```

Most of the parameters are just passed through or hard-wired based on the type of list items. The `itemContent` parameter is the last declared, so it can be moved outside the `(...)` to define the item content. It's also fine to keep it inside the `(...)` - for example:

```kotlin
@Composable
fun ActorList(
    actors: List<ActorDto>,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    onActorClick: (String) -> Unit,
) = ListScaffold(
    titleId = R.string.screen_title_actors,
    items = actors,
    getId = { it.id },
    onSelectListScreen = onSelectListScreen,
    onResetDatabase = onResetDatabase,
    onItemClick = onActorClick,
    itemIcon = Icons.Default.Person,
    itemIconContentDescriptionId = R.string.tap_to_toggle_selection,
    itemContent = { actor ->
        SimpleText(text = actor.name)
    }
)
```

I tend to prefer to have body "content" outside the parens. In this case, the `ListScaffold` manages the "body", so the `itemContent` can appear outside.

Our UI now looks like

![Movie List](screenshots/lists-1.png)