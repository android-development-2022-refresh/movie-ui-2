---
title: Selection and Deletion
template: main-repo.html
---

Now the real fun begins... Selecting items on the main list screens and deleting them from the database.

Let's start in the **Data Layer**. We're going to have a set of ids of items we want to delete. We can either look up all the items and then pass them to our existing `delete` functions, or, we can create new DAO functions that will delete by ids. I prefer the latter, as we don't need to do additional lookups.

## DAO changes

In the DAO, we add

```kotlin
@Query("DELETE FROM Movie WHERE id IN (:ids)")
abstract suspend fun deleteMoviesById(ids: Set<String>)
@Query("DELETE FROM Actor WHERE id IN (:ids)")
abstract suspend fun deleteActorsById(ids: Set<String>)
@Query("DELETE FROM Rating WHERE id IN (:ids)")
abstract suspend fun deleteRatingsById(ids: Set<String>)
```

## Repository Changes

We add simple pass-throughs in the repository, making them available in the view model.

## View Model Changes

Things get a little more interesting here. We need to track what's selected on the screen.

We could add the selection information to the `Screen` objects that allow selection, but then we'd need to be able to update the `Screen` objects or replace the top `Screen` on the stack.

Instead, we decide that because only one list `Screen` is visible at a time, _and_ we cannot navigate when items are selected, we'll just keep a single selection set in the view model:

```kotlin
var selectedItemIds by mutableStateOf<Set<String>>(emptySet())
    private set
```

Using Compose `State` means that whenever we update the value, Compose will trigger recomposition on users of that data, if needed. _However_, to keep the view model consistent, we must make sure we clear the selected items whenever we navigate:

!!! note

    If the selected items were stored in the `Screen` we wouldn't need this clear step.

```kotlin
private var screenStack: List<Screen> = listOf(MovieList)
    set(value) {
        field = value
        screen = value.lastOrNull()
        clearSelections()
    }

fun clearSelections() {
    selectedItemIds = emptySet()
}
```

We've defined a `clearSelections` function that does what it says, and call it whenever the `screenStack` changes.

We'll finish out the functions we need to manage the selections and delete entities:

```kotlin
fun toggleSelection(id: String) {
    selectedItemIds =
        if (id in selectedItemIds) {
            selectedItemIds - id
        } else {
            selectedItemIds + id
        }
}
fun deleteSelectedActors() {
    viewModelScope.launch {
        repository.deleteActorsById(selectedItemIds)
        clearSelections()
    }
}
fun deleteSelectedMovies() {
    viewModelScope.launch {
        repository.deleteMoviesById(selectedItemIds)
        clearSelections()
    }
}
fun deleteSelectedRatings() {
    viewModelScope.launch {
        repository.deleteRatingsById(selectedItemIds)
        clearSelections()
    }
}
```

The `toggleSelection` function will be called when the user interacts with an item:

   * If they click its icon, we call toggleSelection
   * If they long-press anywhere on a row, call toggleSelection
   * If the click anywhere else in the row
      * If there are no selections, navigate to the item detail
      * If there are selections, treat it as a toggle

The delete functions kick off a coroutine to do the deletions and then clear the selection set.

## List and Movie Scaffold Changes

Here's where the interesting work happens. We pass in some new parameters

```kotlin
@Composable
fun <T> ListScaffold(
    ...
    selectedItemIds: Set<String>,           // the current selections
    onClearSelections: () -> Unit,          // tell the caller to clear
    onToggleSelection: (String) -> Unit,    // toggle an item's id
    onDeleteSelectedItems: () -> Unit,      // delete all selected items
    ...
)
```

Any screen that wants to display a top-level list must tell us what's selected, how to update selections and delete the selected items.

Some of this info gets passed to `MovieScaffold`:

```kotlin
@Composable
fun <T> ListScaffold(
    ...
) = MovieScaffold(
    ...
    selectedItemCount = selectedItemIds.size,
    ...
    onDeleteSelectedItems = onDeleteSelectedItems,
    onClearSelections = onClearSelections,
)
```

`MovieScaffold` is pretty straightforward:

   * If `selectedItemCount` is zero, display normal top app bar
   * If `selectedItemCount` is non-zero, display contextual top app bar
      * Has "back" arrow icon - calls `onClearSelections` when pressed
      * Shows `selectedItemCount`
      * Has "delete" action - calls `onDeleteSelectedItems` when pressed

The rest of `ListScaffold` has some interesting parts:

   * We select a `backgroundColor` and `contentColor` based on whether or not an item is selected. (An item is considered selected if its id is in `selectedItemIds`)
      
   * We define a `pointerInput` modifier to process user taps and long-presses, calling the appropriate functions that were passed into `ListScaffold`.

   * We set the `backgroundColor` on the `Card` (and remove it from the `Row`), based on the selection status.

   * We set the colors for the `Icon` background and foreground, again based on the selection status, and make the icon clickable.

!!! note

    The `contentColorFor` function is a great helper that automates the selection of a color on top of a background color

!!! caution

    The lambda that defines the gesture handler only runs once, and then again whenever the key passed to `pointerInput` changes. The gesture handler would capture the value of `selectedItemIds` - if the selections change, the gesture handler would never know of it.

    We could either pass `selectedItemIds` as a key to `pointerInput`, forcing it to redefine the gesture handler _every_ time the selections change, or, we can create an updateable bucket in the composition tree for the selections. We'll use the latter to avoid redefining the gesture handlers.



## Top-Level Ui Changes

Finally, we wire the screens to the view model, passing in the selected ids and the new view model functions to clear/toggle selections and delete selected items.

## But there's a problem...

When we run the code from this step, we can select and delete items. But we run into trouble when we try to look at roles that reference deleted data.

For example, if we delete "The Transporter", then try to view "Jason Statham", we get 

```
E/AndroidRuntime: FATAL EXCEPTION: main
    Process: com.javadude.movies, PID: 17550
    java.lang.NullPointerException: Parameter specified as non-null is null:
       method kotlin.jvm.internal.Intrinsics.checkNotNullParameter,
       parameter movie
```

The problem is that The Transporter is still referenced by `Role` entities. When we try to load Jason Statham's filmography, Room cannot resolve the reference and throws an exception.

Our database is now inconsistent!