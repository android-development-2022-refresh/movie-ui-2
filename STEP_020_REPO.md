---
title: Scrolling Columns and LazyList
template: main-repo.html
---

We start with a copy of our Movies app at the end of the 
"Movie UI, part 1" module.

First, we'll add scrolling to all display screens, just in case they have too much to fit on one screen. We do this by adding a `verticalScroll` modifier to each `Column`:

```kotlin
Column(
    modifier = Modifier
        .padding(paddingValues)
        .verticalScroll(rememberScrollState())
) {
```

The current scroll position is tracked in a `ScrollState` object. The `rememberScrollState` saves a `ScrollState` instance in a slot in the composition tree. It uses `rememberSaveable` rather than remember - this automatically saves and reloads the state across configuration changes.

!!! note

    `rememberSaveable` saves the state _outside_ of the view model, but the data is converted into binary form and then recreated after the new activity instance has been created. This is useful for state that isn't related to data from the data layer, state that only manages the details of a component (such as scroll position). You can use this for your own data; however, you must define a `Saver` that controls the marshaling to/from binary. We won't go into any more detail on this in this course, but know it's available if you're defining your own composable functions that need some managed data that you don't want to put into a view model.

Next, we convert our main lists to use `LazyColumn` with `Cards` instead of just a `Column` of `Text`:

Old code (Actor, for example):

```kotlin
Column(modifier = Modifier.padding(paddingValues)) {
    actors.forEach {
        SimpleText(text = it.name) {
            onActorClick(it.id)
        }
    }
}
```

New Code

```kotlin
LazyColumn(
    modifier = Modifier.padding(paddingValues)
) {
    items(
        items = actors,
        key = { it.id },
    ) {
        Card(
            elevation = 4.dp,
            modifier = Modifier
                .padding(8.dp)
                .fillMaxWidth()
        ) {
            SimpleText(text = it.name) {
                onActorClick(it.id)
            }
        }
    }
}
```

This allows Compose to manage the list of actors/movies/ratings dynamically, only emitting the items that are visible or needed for scrolling/animation. The `Cards` are only here to make things look a little more interesting; they're never required.