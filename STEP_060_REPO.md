---
title: Referential Integrity
template: main-repo.html
---

One solution would be to explicitly delete any `Roles` that reference `Movies` or `Actors` that we're deleting. A good bit of work...

Fortunately, there's another solution that Room (and the database behind it) support: Referential Integrity.

We can define some rules to either automatically delete entities that reference deleted data, or disallow deleting the referenced items until the references are deleted. We'll go the first route.

A `Role` doesn't make sense if either the `Movie` or `Actor` has been deleted. We will set up a "cascading deletion" - when the referenced `Movie` or `Actor` is deleted, we ask Room to delete the `Role`:

```kotlin
@Entity(
    primaryKeys = ["actorId", "movieId"],
    indices = [
        Index(value = ["movieId"]),
        Index(value = ["actorId"]),
    ],
    foreignKeys = [
        ForeignKey(
            entity = Actor::class,
            parentColumns = ["id"],
            childColumns = ["actorId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
        ),
        ForeignKey(
            entity = Movie::class,
            parentColumns = ["id"],
            childColumns = ["movieId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
        ),
    ]
)
data class Role(
    var movieId: String,
    var actorId: String,
    var character: String,
    var orderInCredits: Int,
)
```

This updated `@Entity` annotation does the following:

   * We add indexes to improve lookup performance
   * We define explicit `ForeignKeys` to declare how our `actorId` and `movieId` are associated with the `Actor` and `Movie` entities. 
      * The `entity` states a _parent_ entity. In this case, we treat it as an _owner_, an entity that must exist for our `Role` to make sense.
      * `onUpdate` says "if the `id` of the parent changes, update my corresponding reference `id`". For example, if the referenced movie's `id` changes, change my `movieId`.
      * `onDelete` says "if the referenced parent is deleted, delete me". This works for either the `Movie` or `Actor` being referenced.

This fixes our problem!

!!! note

    When you change the entity definition like this, the database on disk no longer matches the definition. You'll see something like the following when you run the updated application:

    ```
    java.lang.IllegalStateException: Room cannot verify the data integrity.
      Looks like you've changed schema but forgot to update the version number.
      You can simply fix this by increasing the version number.
    ```
    
    You'll either need to increment the database version (in `MovieDatabase.kt`), or uninstall and reinstall the application (which will delete the database file and recreate it).

    If you haven't published the application, I'd recommend just uninstalling and reinstalling the app while you're testing. If you've deployed, you'll need to increase the database version and likely set up migration rules (which is beyond the scope of this course, but easily found online). 

